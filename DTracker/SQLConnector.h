#pragma once

#include <QObject>
#include <QDateTime>


class SQLConnector : public QObject {
Q_OBJECT

public:
    SQLConnector(QObject *parent = nullptr);

    static void addData(QString process, int diff);

    static int getProcessID(QString procname);

    static void addNewProcessID(QString procname);

    static bool isProcessExists(QString procname);

    static int getProcessesCount();

    static QVariantList getProcesses();

    static void saveProcesses(QVariantList list);

    static int getProcessTime(QDateTime startTime, QDateTime endTime = QDateTime::currentDateTime());

    static QVariantList getAllProcesses(QDate oldTime, QDate newTime, int val);

    static int64_t getTimeForPeriod(QDate startTime, QDate endTime);
};
