#include "SQLConnector.h"
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
#include <QStandardPaths>
#include <QDir>
#include <QtWidgets/QtWidgets>
#include "DSettings.h"

SQLConnector::SQLConnector(QObject *parent)
        : QObject(parent) {
    QSqlDatabase m_db = QSqlDatabase::addDatabase("QSQLITE");
    QString dataLocaltion = QStandardPaths::standardLocations(QStandardPaths::DataLocation).first();
    qDebug() << dataLocaltion;
    if (!QDir().exists(dataLocaltion)) QDir().mkpath(dataLocaltion);
    m_db.setDatabaseName(qApp->applicationDirPath() + "/cache.sqlite");
    if (!m_db.open()) qCritical() << "DB failed";
    else DSettings::writeLog("Database successfully opened");
    m_db.exec(
            "CREATE TABLE IF NOT EXISTS \"timings\" ( `date` INTEGER NOT NULL, `proc_id` INTEGER, `time` INTEGER, PRIMARY KEY(`date`,`proc_id`) )");
    m_db.exec(
            "CREATE TABLE IF NOT EXISTS `processes` ( `id` INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, `execname` TEXT UNIQUE NOT NULL, `fullname` TEXT, `strength`	INTEGER DEFAULT 2)");
}

void SQLConnector::addData(QString process, int diff) {
    QSqlDatabase db = QSqlDatabase::database();
    if (!isProcessExists(process)) addNewProcessID(process);
    int proc_id = getProcessID(process);
    QDateTime currentDateTime = QDateTime::currentDateTime();
    QString currentDate = currentDateTime.toString("yyyyMMdd");
    QSqlQuery query;
    query.prepare("SELECT time FROM timings WHERE proc_id = :proc_id AND date = :date");
    query.bindValue(":date", currentDate.toInt());
    query.bindValue(":proc_id", proc_id);
    query.exec();
    int time = 0;
    if (query.next()) {
        time = query.value(0).toInt();
    }
    query.prepare("INSERT OR IGNORE INTO timings (date, proc_id, time) VALUES(:date, :proc_id, :time)");
    query.bindValue(":date", currentDate.toInt());
    query.bindValue(":proc_id", proc_id);
    query.bindValue(":time", time + diff);
    query.exec();
    query.prepare("UPDATE timings SET time = :newtime WHERE proc_id = :newprocid AND date = :newdate");
    query.bindValue(":newtime", time + diff);
    query.bindValue(":newprocid", proc_id);
    query.bindValue(":newdate", currentDate.toInt());
    query.exec();
}

int SQLConnector::getProcessID(QString procname) {
    QSqlDatabase db = QSqlDatabase::database();
    auto procid_res = db.exec("SELECT id FROM processes WHERE execname = \"" + procname + "\";");
    if (procid_res.next()) {
        return procid_res.value(0).toInt();
    }
    return -1;
}

void SQLConnector::addNewProcessID(QString procname) {
    QSqlQuery query;
    query.prepare("INSERT INTO processes (execname, fullname) "
                  "VALUES (:execname, :fullname)");
    query.bindValue(":execname", procname);
    query.bindValue(":fullname", "");
    query.exec();
}

bool SQLConnector::isProcessExists(QString procname) {
    QSqlDatabase db = QSqlDatabase::database();
    auto res = db.exec("SELECT COUNT(*) FROM processes WHERE execname = \"" + procname + "\";");
    if (res.next()) {
        return res.value(0).toInt();
    }
    return false;
}

int SQLConnector::getProcessesCount() {
    QSqlDatabase db = QSqlDatabase::database();
    auto res = db.exec("SELECT COUNT(*) FROM processes;");
    if (res.next()) {
        return res.value(0).toInt();
    }
    return 0;
}

QVariantList SQLConnector::getProcesses() {
    QSqlDatabase db = QSqlDatabase::database();
    auto res = db.exec("SELECT * FROM processes;");
    QVariantList list;
    while (res.next()) {
        QVariantMap map;
        map["procname"] = res.value(1).toString();
        map["fullprocname"] = res.value(2).toString();
        map["strength"] = res.value(3).toInt();
        list.append(map);
    }
    return list;
}

void SQLConnector::saveProcesses(QVariantList list) {
    QSqlDatabase db = QSqlDatabase::database();
    QVariantList map = getProcesses();
    for (int i = 0; i < map.size(); i++) {
        QVariantMap oldElem = qvariant_cast<QVariantMap>(map.at(i));
        QVariantMap newElem = qvariant_cast<QVariantMap>(list.at(i));
        if (newElem != oldElem) {
            QSqlQuery query;
            query.prepare(
                    "UPDATE processes SET fullname = :fullname, strength = :strength WHERE execname = :execname;");
            query.bindValue(":execname", oldElem["procname"].toString());
            query.bindValue(":fullname", newElem["fullprocname"].toString());
            query.bindValue(":strength", newElem["strength"].toInt());
            query.exec();
        }
    }
}

int SQLConnector::getProcessTime(QDateTime startTime, QDateTime endTime) {
    return 0;
}

bool variantLessThan(const QVariantMap &v1, const QVariantMap &v2) {
    return v1["time"].toInt() > v2["time"].toInt();
}

QVariantList SQLConnector::getAllProcesses(QDate oldTime, QDate newTime, int val) {
    int endTimeInt = newTime.toString("yyyyMMdd").toInt();
    int startTimeInt = oldTime.toString("yyyyMMdd").toInt();
    QSqlQuery query;
    query.prepare(
            "SELECT proc_id, time, processes.execname, processes.fullname FROM timings INNER JOIN processes on timings.proc_id = processes.id WHERE strength = :val AND date >= :startdate AND date <= :enddate");
    query.bindValue(":startdate", startTimeInt);
    query.bindValue(":enddate", endTimeInt);
    query.bindValue(":val", val);
    query.exec();
    QMap<QString, QVariantMap> map;
    while (query.next()) {
        map[query.value(2).toString()]["time"] =
                map[query.value(2).toString()]["time"].toInt() + query.value(1).toInt();
        map[query.value(2).toString()]["procname"] = query.value(2).toString();
        map[query.value(2).toString()]["fullprocname"] = query.value(3).toString();
    }
    QList<QVariantMap> list;
    for (auto e : map.keys()) {
        QVariantMap mp;
        mp["time"] = map.value(e)["time"].toInt();
        mp["procname"] = map.value(e)["procname"].toString();
        mp["fullprocname"] = map.value(e)["fullprocname"].toString();
        list.push_back(mp);
    }
    qSort(list.begin(), list.end(), variantLessThan);
    QVariantList jsList;
    for (int i = 0; i < map.size() && i < 5; i++) {
        QVariantMap jsMap;
        jsMap["procname"] = list[i]["procname"].toString();
        jsMap["time"] = list[i]["time"].toInt();
        jsMap["fullprocname"] = list[i]["fullprocname"].toString();
        jsList.push_back(jsMap);
    }
    return jsList;
}

int64_t SQLConnector::getTimeForPeriod(QDate startTime, QDate endTime) {
    int endTimeInt = endTime.toString("yyyyMMdd").toInt();
    int startTimeInt = startTime.toString("yyyyMMdd").toInt();
    QSqlQuery query;
    query.prepare("SELECT time FROM timings WHERE date >= :startdate AND date <= :enddate");
    query.bindValue(":startdate", startTimeInt);
    query.bindValue(":enddate", endTimeInt);
    query.exec();
    int64_t time = 0;
    while (query.next()) {
        time += query.value(0).toInt();
    }
    return time;
}
