#pragma once

#include <QtWidgets/QMainWindow>
#include <windows.h>
#include <QTime>
#include <QVariant>
#include <QTimer>

class DTracker : public QObject {
Q_OBJECT

public:

    explicit DTracker(QObject *parent = nullptr);

    static DTracker &object() {
        static DTracker tracker;
        return tracker;
    }

    static VOID CALLBACK
    windowChanged(HWINEVENTHOOK hWinEventHook, DWORD dwEvent, HWND hwnd, LONG idObject, LONG idChild,
                  DWORD dwEventThread, DWORD dwmsEventTime);

    static void receivedWindowsEvent(WPARAM status);

    Q_INVOKABLE void saveProcess(QString name, QString strength, QString fullname);

    Q_INVOKABLE void saveProcesses(QVariantList list);

    Q_INVOKABLE QVariantList getProcesses();

    Q_INVOKABLE int getProcessesCount();

    Q_INVOKABLE QVariantList getProductiveProcesses(QDate oldDate, QDate newDate);

    Q_INVOKABLE QVariantList getDistractiveProcesses(QDate oldDate, QDate newDate);

    Q_INVOKABLE QVariantList getAnyProcesses(QDate oldDate, QDate newDate);

    Q_INVOKABLE QString getTimeForPeriod(QDate startDate, QDate endDate);

    QTime currentProcessStart;
    QString currentProcess;
    QTimer m_timer;

signals:

    void updateValues();

private:
};
