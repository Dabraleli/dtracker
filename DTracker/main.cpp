#include "DTracker.h"
#include <QtWidgets/QApplication>
//#include <QQuickStyle>
#include "SQLConnector.h"
#include <QDebug>
#include <psapi.h>
#include <QAbstractEventDispatcher>
#include "WinEventFilter.h"
#include <QStandardPaths>
#include <QSettings>
#include <dwmapi.h>
#include <QSystemTrayIcon>
#include "DSettings.h"

Q_DECLARE_METATYPE(QSystemTrayIcon::ActivationReason)

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    DSettings::startLog();
    DSettings::writeLog("Starting DTracker...");


    QAbstractEventDispatcher::instance()->installNativeEventFilter(new WinEventFilter);

    QStringList args = QCoreApplication::arguments();
    if (args.count() == 1 || (args.count() == 2 && QCoreApplication::arguments().at(1) != "minimized")) {
        //engine.rootContext()->setContextProperty("winState", "windowed");
    }
    //else engine.rootContext()->setContextProperty("winState", "minimized");

    DSettings::writeLog("DTracker ready");
    return app.exec();
}
