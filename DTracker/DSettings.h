#pragma once

#include <QObject>
#include <QSettings>

class DSettings : public QObject {
Q_OBJECT
    Q_PROPERTY(bool settingHideToTray
                       READ
                       settingHideToTray
                       WRITE
                       setHideToTray
                       NOTIFY
                       hideToTrayChanged)
    Q_PROPERTY(bool settingStartWithWindows
                       READ
                       settingStartWithWindows
                       WRITE
                       setStartWithWindows
                       NOTIFY
                       startWithWindowsChanged)
    Q_PROPERTY(bool settingDebugInfo
                       READ
                       settingDebugInfo
                       WRITE
                       setDebugInfo
                       NOTIFY
                       debugInfoChanged)

public:
    explicit DSettings(QObject *parent = nullptr);

    ~DSettings();

    bool settingHideToTray();

    bool settingStartWithWindows();

    bool settingDebugInfo();

    void setHideToTray(bool val);

    void setStartWithWindows(bool val);

    void setDebugInfo(bool val);

    void setLaunchOnStartup(bool res);

    Q_INVOKABLE static void writeLog(QString string);

    static void startLog();

signals:

    void hideToTrayChanged();

    void startWithWindowsChanged();

    void debugInfoChanged();

private:
    bool m_hideToTray;
    bool m_startWithWindows;
    bool m_debugInfo;
    QSettings *m_settings;
};
