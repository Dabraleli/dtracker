#pragma once

#include <QAbstractNativeEventFilter>
#include <QAbstractEventDispatcher>
#include <QDebug>
#include <windows.h>
#include "DTracker.h"

class WinEventFilter : public QAbstractNativeEventFilter {
public:
    virtual bool nativeEventFilter(const QByteArray &eventType, void *message, long *) Q_DECL_OVERRIDE {
        MSG *msg = static_cast< MSG * >(message);

        if (msg->message == WM_POWERBROADCAST) {
            DTracker::receivedWindowsEvent(msg->wParam);
        }
        return false;
    }
};
