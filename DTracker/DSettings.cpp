#include "DSettings.h"
#include <QSettings>
#include <QCoreApplication>
#include <QFile>
#include <QStandardPaths>
#include <QDir>
#include <QDateTime>
#include <qtextstream.h>
#include <QDebug>

DSettings::DSettings(QObject *parent)
        : QObject(parent) {
    m_settings = new QSettings("DTracker", "DTracker");
    setHideToTray(m_settings->value("hideToTray", true).toBool());
    setStartWithWindows(m_settings->value("startWithWindows", false).toBool());
    setDebugInfo(m_settings->value("debug", false).toBool());
    setLaunchOnStartup(m_settings->value("startWithWindows", false).toBool());
}

DSettings::~DSettings() {
    m_settings->deleteLater();
}

bool DSettings::settingHideToTray() {
    return m_settings->value("hideToTray", true).toBool();
}

bool DSettings::settingStartWithWindows() {
    return m_settings->value("startWithWindows", false).toBool();
}

bool DSettings::settingDebugInfo() {
    return m_settings->value("debug", false).toBool();
}

void DSettings::setHideToTray(bool val) {
    if (val == m_hideToTray) return;
    m_hideToTray = val;
    m_settings->setValue("hideToTray", m_hideToTray);
    emit hideToTrayChanged();
}

void DSettings::setStartWithWindows(bool val) {
    if (val == m_startWithWindows) return;
    m_startWithWindows = val;
    m_settings->setValue("startWithWindows", m_startWithWindows);
    setLaunchOnStartup(m_startWithWindows);
    emit startWithWindowsChanged();
}

void DSettings::setDebugInfo(bool val) {
    if (val == m_debugInfo) return;
    m_debugInfo = val;
    m_settings->setValue("debug", m_debugInfo);
    emit debugInfoChanged();
}

void DSettings::setLaunchOnStartup(bool res) {
    QSettings settings("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);
    if (res) {
        settings.setValue("DTracker", "\"" +
                                      QCoreApplication::applicationFilePath().replace('/', '\\') + "\" minimized");
    } else {
        settings.remove("DTracker");
    }
}

void DSettings::writeLog(QString string) {
    qDebug() << "[" << QDateTime::currentDateTime().toString("hh:mm:ss dd.MMM") << "] " << string;;
    if (QSettings("DTracker", "DTracker").value("debug", false).toBool()) {
        QString dataLocaltion = QStandardPaths::standardLocations(QStandardPaths::DataLocation).first();
        if (!QDir().exists(dataLocaltion)) QDir().mkpath(dataLocaltion);
        QFile logFile(qApp->applicationDirPath() + "/log.txt");
        logFile.open(QIODevice::WriteOnly | QIODevice::Append);
        QDateTime date = QDateTime::currentDateTime();
        QTextStream outStream(&logFile);
        outStream << "[" << date.toString("hh:mm:ss dd.MMM") << "] " << string << "\r\n";
        logFile.close();
    }
}

void DSettings::startLog() {
    QString dataLocaltion = QStandardPaths::standardLocations(QStandardPaths::DataLocation).first();
    if (!QDir().exists(dataLocaltion)) QDir().mkpath(dataLocaltion);
    QFile logFile(dataLocaltion + "/log.txt");
    logFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QDateTime date = QDateTime::currentDateTime();
    QTextStream outStream(&logFile);
    outStream << "\r\n\r\n";
    logFile.close();
}
