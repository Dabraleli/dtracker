#include <windows.h>
#include "DTracker.h"
#include <QDebug>
#include <windows.h>
#include <psapi.h>
#include "SQLConnector.h"
#include <QTime>
#include "DSettings.h"

DTracker::DTracker(QObject *parent) :
        QObject(parent) {
    m_timer.setInterval(5000);
    m_timer.setSingleShot(true);
    connect(&m_timer, &QTimer::timeout, [this]() {
        char Buffer1[MAX_PATH];
        QString newFname;
        DWORD dwPID2;
        GetWindowThreadProcessId(GetForegroundWindow(), &dwPID2);

        HANDLE Handle2 = OpenProcess(
                PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
                FALSE,
                dwPID2
        );
        if (GetModuleFileNameEx(Handle2, 0, Buffer1, MAX_PATH)) {
            newFname = Buffer1;
        }
        newFname = newFname.split("\\").last();
        if (currentProcess != newFname) {
            DSettings::writeLog("Process diff found: " + newFname + " " + this->currentProcess);
            this->currentProcess = newFname;
        }
    });
    SetWinEventHook(EVENT_SYSTEM_FOREGROUND,
                    EVENT_SYSTEM_FOREGROUND, NULL,
                    DTracker::windowChanged, 0, 0,
                    WINEVENT_OUTOFCONTEXT | WINEVENT_SKIPOWNPROCESS);
}

void DTracker::windowChanged(HWINEVENTHOOK hWinEventHook, DWORD dwEvent, HWND hwnd, LONG idObject, LONG idChild,
                             DWORD dwEventThread, DWORD dwmsEventTime) {
    QString filename;
    object().m_timer.start();
    DWORD dwPID;
    GetWindowThreadProcessId(hwnd, &dwPID);

    HANDLE Handle = OpenProcess(
            PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
            FALSE,
            dwPID
    );
    if (Handle) {
        char Buffer[MAX_PATH];
        if (GetModuleFileNameEx(Handle, 0, Buffer, MAX_PATH)) {
            filename = Buffer;
        }
        CloseHandle(Handle);
    }
    filename = filename.split("\\").last();
    if (!filename.contains(".exe")) return;
    if (object().currentProcess != filename) {
        if (object().currentProcess != "") {
            if (!SQLConnector::isProcessExists(filename)) SQLConnector::addNewProcessID(filename);
            SQLConnector::addData(object().currentProcess, object().currentProcessStart.msecsTo(QTime::currentTime()));
            DSettings::writeLog(object().currentProcess + " took " +
                                QString::number(object().currentProcessStart.secsTo(QTime::currentTime())) + "s");
        }
        object().currentProcess = filename;
        object().currentProcessStart = QTime::currentTime();
        DSettings::writeLog("Current process = " + filename);
        emit object().updateValues();
    }
}

void DTracker::receivedWindowsEvent(WPARAM status) {
    if (status == PBT_APMRESUMEAUTOMATIC) {
        DSettings::writeLog("Resuming from sleep");
        DTracker::object().currentProcessStart = QTime::currentTime();
    }
}

void DTracker::saveProcess(QString name, QString strength, QString fullname) {

}

void DTracker::saveProcesses(QVariantList list) {
    SQLConnector::saveProcesses(list);
}

QVariantList DTracker::getProcesses() {
    return SQLConnector::getProcesses();
}

int DTracker::getProcessesCount() {
    return SQLConnector::getProcessesCount();
}

QVariantList DTracker::getProductiveProcesses(QDate oldDate, QDate newDate) {
    return SQLConnector::getAllProcesses(oldDate, newDate, 4);
}

QVariantList DTracker::getDistractiveProcesses(QDate oldDate, QDate newDate) {
    return SQLConnector::getAllProcesses(oldDate, newDate, 0);
}

QVariantList DTracker::getAnyProcesses(QDate oldDate, QDate newDate) {
    QVariantList list;
    for (int i = 0; i < 5; i++) {
        QVariantList newList = SQLConnector::getAllProcesses(oldDate, newDate, i);
        int64_t time = 0;
        for (int j = 0; j < newList.size(); j++) {
            QVariantMap oldElem = qvariant_cast<QVariantMap>(newList.at(j));
            time += oldElem["time"].toInt();
        }
        list.append(time);
    }
    return list;
}


QString DTracker::getTimeForPeriod(QDate startDate, QDate endDate) {
    int64_t time = SQLConnector::getTimeForPeriod(startDate, endDate);
    int minutes = (int) ((time / (1000 * 60)) % 60);
    int hours = (int) ((time / (1000 * 60 * 60)) % 24);
    return QString::number(hours) + ":" + (minutes < 10 ? "0" : "") + QString::number(minutes);
}